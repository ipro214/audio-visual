import urllib
import scipy.io.wavfile
import pyaudio
import pydub
import numpy as np


from numpy import fft as fft

rate,audData=scipy.io.wavfile.read("trumpet.wav")
channel1=audData[:,0] #left
channel2=audData[:,1] #right
fourier=fft.fft(channel1)
n = len(channel1)/2
n= int(n)
fourier = fourier[0:n]
n = len(channel1)/2
fourier = fourier / float(n)
freqArray = np.arange(0, (n/2), 1.0) * (rate*1.0/n);
freqArray/1000
dB= 10*np.log10(fourier)
print(dB)
