import numpy as np
import threading
import time
import wave
import sys
import matplotlib.pyplot as plt
import os

from threading import Thread
from numpy import fft


def getDB1():    #returns a 0 if no strange noise level is reached and 1 if it is
    spf = wave.open('claptest.wav','r')     #makes a variable equal to the audio file
    print('getdb starts')
    signal = spf.readframes(-1)             #reads frames of data from the audio file
    signal = np.fromstring(signal,'Int16')  #reads the string files from the previous line and converts to Int16

    fs = spf.getframerate()                 #gets the framerate of the audio
    Time = np.linspace(0, len(signal)/fs, num = len(signal))    #linspace returns evenly spaced numbers over a specified interval, returns num evenly spaced samples calculated over the duration of the sample.
    loudcount = 0
    timing = []
    count = 0
    for x in signal:        #goes through every data point
        if x > 6500:        #if a data point exceeds our threshhold
            print('returning 1')
            return 1

    return 0

def getDB2():
    spf = wave.open('claptest1.wav','r')
    print('getdb starts')
    signal = spf.readframes(-1)
    signal = np.fromstring(signal,'Int16')
    fs = spf.getframerate()
    Time = np.linspace(0, len(signal)/fs, num = len(signal))
    loudcount = 0
    timing = []
    count = 0
    for x in signal:
        if x > 6500:
            print('returning 1')
            return 1

    return 0


def getPIC():   #returns a 0 if no strange flash is detected and 1 if one is
    print('getpic starting')


    return 0

def Alert():    #stops our process and runs an emergency situation protocol
    check = 0


class MyThread(threading.Thread):       #This is a class used by the child of the main process
    def run(self):                      #This defines what happens when the child starts
        print('New MyThread Starting')
        if flg == 0:                    #runs getDB
            x = getDB1()
        else:
            x = getDB2()
        if x == 1:                      #if getDB returned a 1, run getPIC
            x = getPIC()
            if x == 1:                  #if getPIC returned 1, run Alert
                print('Sending Alert')
                Alert()
        time.sleep(1)

check = 1
p = 0
flg = 0
if __name__ == '__main__':
    while(check == 1):                  #Check only changes if we have an alert
        p+=1
        t = MyThread(name = "Thread-{}".format(p+1))    #Creates a thread that will run
        t.start()                                       #starts thread and starts the run function inside the thread class
        if flg == 0:
            os.popen('cp claptest.wav claptest1.wav')
            os.remove('claptest.wav')
            flg = 1
        else:
            os.popen('cp claptest1.wav claptest.wav')
            os.remove('claptest1.wav')
            flg = 0
        time.sleep(5)                                   #waits to process more data
        print(p)
